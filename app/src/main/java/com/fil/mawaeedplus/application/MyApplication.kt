package com.fil.mawaeedplus.application



import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.facebook.drawee.backends.pipeline.Fresco

import com.google.firebase.FirebaseApp






class MyApplication : Application() {


    companion object {
        lateinit var instance: MyApplication


    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        FirebaseApp.initializeApp(this)
        Fresco.initialize(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }


}