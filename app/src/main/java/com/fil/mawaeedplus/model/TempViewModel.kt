package com.fil.mawaeedplus.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.mawaeedplus.api.RestCallback
import com.fil.mawaeedplus.api.RestClient
import com.fil.mawaeedplus.pojo.UserData

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TempViewModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<UserData>>
    lateinit var mContext: Context
    var isLoginApi: Boolean = false

    var searchkeyword: String = ""
    var json: String = ""
    var header: String = ""


    fun TempViewModel(
        context: Context,
        isCustomerDetails: Boolean,
        json: String,
        header: String
    ): LiveData<List<UserData>> {

        this.mContext = context
        this.isLoginApi = isCustomerDetails
        this.searchkeyword = searchkeyword
        this.json = json
        this.header = header
        languageresponse = checkUserNameApi()

        return languageresponse
    }

    private fun checkUserNameApi(): LiveData<List<UserData>> {
        val data = MutableLiveData<List<UserData>>()
        var call = RestClient.get()!!.verifyOtp( json)

        call.enqueue(object :RestCallback<List<UserData>>(mContext){
            override fun Success(response: Response<List<UserData>>) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun failure() {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        })
        return data
    }


}