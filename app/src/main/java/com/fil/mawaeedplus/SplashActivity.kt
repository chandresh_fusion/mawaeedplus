package com.fil.mawaeedplus

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.fil.mawaeedplus.util.MyUtils

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            MyUtils.startActivity(this,MainActivity::class.java,true)

        },2000)
    }
}
