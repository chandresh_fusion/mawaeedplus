package com.fil.mawaeedplus.pojo

import java.io.Serializable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by Chandresh
 * FIL
 */
class Push : Serializable {
    companion object {
        val  action:String="Push"
        val bokingRequest:String="BookingRequest"
    }

    @SerializedName("RefKey")
    @Expose
    var refKey: String? = ""
    @SerializedName("priority")
    @Expose
    var priority: String? = ""
    @SerializedName("msgKey")
    @Expose
    var msgKey: String? = ""
    @SerializedName("msg")
    @Expose
    var msg: String? = ""
    @SerializedName("body")
    @Expose
    var body: String? = ""
    @SerializedName("type")
    @Expose
    var type: String? = ""
    @SerializedName("title")
    @Expose
    var title: String? = ""
    @SerializedName("msgType")
    @Expose
    var msgType: String? = ""

    @SerializedName("RefData")
    @Expose
    var `RefData`: String?=""


}
data class RefData (
    @SerializedName("bookingDropAddress")
    var bookingDropAddress: String? = "",
    @SerializedName("bookingDroplatlong")
    var bookingDroplatlong: String? = "",
    @SerializedName("bookingID")
    var bookingID: String? = "",
    @SerializedName("bookingPickupAddress")
    var bookingPickupAddress: String? = "",
    @SerializedName("bookingPickuplatlong")
    var bookingPickuplatlong: String? = "",
    @SerializedName("categoryID")
    var categoryID: String? = "",
    @SerializedName("userID")
    var userID: String? = "",
    @SerializedName("bookingType")
    var bookingType: String? = "",
    @SerializedName("bookingDurationEst")
    var bookingDurationEst: String? = "0"
): Serializable
