package com.fil.mawaeedplus.api




import com.fil.mawaeedplus.pojo.UserData
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.GET


public interface RestApi {

    @FormUrlEncoded
    @POST("Mentor/VerifyOTPJson?")
    fun verifyOtp( @Field("json") json: String): Call<List<UserData>>


}
